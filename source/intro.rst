Intro
=====
.. versionadded:: 0.1
.. versionchanged:: 1.2

.. image:: https://img.shields.io/badge/release-0.2-orange.svg

=====
About
=====

TRACKIT is an Asset Management tool, designed to be delivered via the Cloud as a Software as a Service (SaaS) application. It can be deployed locally and installed on a local virtual or physical machine within a Customer environment.

--------------
Placement Type
--------------

To ensure Assets are placed correctly, the Product Model must belong to an appropriate system :ref:`asset_class`.

It is possible for a Product to belong to more than one Asset Class, therefore ensure that the correct class is chosen to determine the placement type, and add any additional classes for reporting/grouping as required.


.. _categories:
