.. TRACKIT documentation master file, created by
   sphinx-quickstart on Tue Feb 07 13:44:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: https://img.shields.io/badge/release-1.3-yellow.svg

TRACKIT APP User Documentation
==============================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   intro



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
